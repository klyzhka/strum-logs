# frozen_string_literal: true

require "spec_helper"

describe "Httparty logger" do
  describe "when successful request with httparty" do
    it "creates logs for success response" do
      stub_request(:post, "http://example.com/").
        with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}, body: {mock: "request"}).
        to_return(:status => 200, :body => "mock response", :headers => {})
      response, logs = logs_response { HTTParty.post('http://example.com/', body: { mock: "request" }) }
      _(response.body).must_equal("mock response")
      _(logs["service_name"]).must_equal("test")
      _(logs["log_status"]).must_equal("success")
      _(logs["status"]).must_equal(200)
      _(logs["method"]).must_equal("POST")
      _(logs["response_message"]).must_equal("mock response")
      _(logs["path"]).must_equal("http://example.com/")
      _(logs["message"]).must_equal("Httparty Request")
    end
  end

  describe "when failure request with httparty" do
    it "creates logs for failure response" do
      stub_request(:get, "http://example.com/").
        with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
        to_return(:status => 404, :body => "mock response", :headers => {})
      response, logs = logs_response { HTTParty.get('http://example.com/') }
      _(response.body).must_equal("mock response")
      _(logs["service_name"]).must_equal("test")
      _(logs["log_status"]).must_equal("error")
      _(logs["status"]).must_equal(404)
      _(logs["method"]).must_equal("GET")
      _(logs["response_message"]).must_equal("mock response")
      _(logs["path"]).must_equal("http://example.com/")
      _(logs["message"]).must_equal("Httparty Request")
    end
  end
end
