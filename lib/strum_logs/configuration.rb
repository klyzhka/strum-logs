# frozen_string_literal: true

require "ougai"
require "strum_logs/errors/configuration_error"

module StrumLogs
  module Configuration
    SUCCESSFUL_STATUSES = (200..299).freeze

    extend Dry::Configurable

    setting(:application_version, "1.0.0") { |value| value }
    setting(:application_name, "application name") { |value| value }
    setting(:level, Logger::INFO) { |value| value }
    setting(:stdout_sync, true) { |value| value }
    setting(:pg_instrumentation, false) { |value| value }
    setting(:redis_instrumentation, false) { |value| value }
    setting(:enable_export_spans, false) { |value| value }
    setting(:environment) { |_value| ENV["RACK_ENV"] || "development" }
    setting(:stack_trace, true) { |value| value }
    setting(:rack_instrumentation, false) { |value| value }
    setting(:faraday_instrumentation, false) { |value| value }
    setting(:rabbit_instrumentation, false) { |value| value }
    setting(:redis_after_call_hooks, []) { |value| value }
    setting(:httparty_after_call_hooks, []) { |value| value }
    setting(:team_name, "team name") { |value| value }
    setting(:black_list_keys, []) { |value| value }
  end
end
