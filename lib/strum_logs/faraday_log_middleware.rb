# frozen_string_literal: true

require "strum_logs/default_logger"
require "strum_logs/helpers/http_log_helper"

module StrumLogs
  module Faraday
    class RequestLogMiddleware
      include StrumLogs::Helpers::HttpLogHelper

      def initialize(app)
        @app = app
        @logger = DefaultLogger.instance.logger
        @tracer = DefaultTelemetry.instance.tracer
      end

      def call(request_env)
        @tracer.in_span(request_env.url.to_s, kind: :client) do |span|
          logging_process(request_env, span)
        end
      end

      def logging_process(request_env, span)
        OpenTelemetry.propagation.inject(request_env.request_headers)
        log_entity = init_log(request_env)
        @app.call(request_env).on_complete do |response_env|
          tracer_info_set(response_env) if Configuration.config.enable_export_spans
          log_data(log_entity, response_env)
          log_status(log_entity, response_env)
        rescue StandardError => e
          error_process(log_entity, e, span)
          raise e
        ensure
          logg_process(log_entity, response_env)
        end
      end

      def log_status(log_entity, env)
        env.success? ? log_entity.merge!({ log_status: "success" }) : log_entity.merge!({ log_status: "error" })
      end

      def log_data(log_entity, response_env)
        log_entity[:status] = response_env.status
        log_entity[:headers] = response_env.response_headers
        log_entity[:response_message] = parse_body(response_env.response_body)
      end

      def init_log(env)
        {
          method: env.method.to_s.upcase,
          peer: env.url.host,
          path: env.url.to_s,
          query: env.url.query,
          protocol: "HTTP",
          started_at: Time.now,
          message: "Faraday HTTP Request"
        }
      end

      def tracer_info_set(response_env)
        span = OpenTelemetry::Trace.current_span(OpenTelemetry::Context.current)
        context = span.context
        response_env.response_headers[:trace_id] = context.hex_trace_id
        response_env.response_headers[:span_id] = context.hex_span_id
      end
    end
  end
end
