# frozen_string_literal: true

require "strum_logs/configuration"

module StrumLogs
  class StrumEsbHooks
    class << self
      def before_publish_hook(body, properties)
        DefaultTelemetry.instance.tracer.in_span("Publishing AMQP message", kind: :producer) do
          OpenTelemetry.propagation.inject(properties[:headers])
          DefaultLogger.logger.info({ message: "Publishing AMQP message",
                                      body: body,
                                      properties: properties,
                                      protocol: "AMQP" })
        end
      end

      def before_handler_hook(_body, _properties, metadata)
        context = OpenTelemetry.propagation.extract(metadata[:headers])
        span = DefaultTelemetry.instance.tracer.start_span("Processing AMQP message", with_parent: context, kind: :consumer)
        context = OpenTelemetry::Trace.context_with_span(span)
        metadata.to_hash[:otel_context_token] = OpenTelemetry::Context.attach(context)
        span.set_attribute("start", Time.now.utc.iso8601)
      end

      def after_handler_hook(body, properties, metadata, payload, error)
        handler_payload(body, properties, metadata, payload, error)
      end

      private

      def handler_payload(body, properties, metadata, payload, error)
        span = OpenTelemetry::Trace.current_span
        log_entity = init_log(payload, body, properties, metadata)
        log(log_entity, error, span)
        span.finish
        OpenTelemetry::Context.detach(metadata[:otel_context_token])
        log_entity
      end

      def log(log_entity, error, span) # rubocop:disable Metrics/AbcSize:
        if error
          set_error(log_entity, error)
          span.record_exception(error)
          span.status = OpenTelemetry::Trace::Status.error(log_entity[:message])
          DefaultLogger.instance.logger.error(log_entity)
        else
          span.status = OpenTelemetry::Trace::Status.ok(log_entity[:message])
          DefaultLogger.instance.logger.info(log_entity)
        end
      end

      def set_error(log_entity, error)
        log_entity[:message] = "Failed to process AMQP message"
        log_entity[:error] = error
        log_entity[:stack_trace] = error.backtrace if Configuration.config.stack_trace
      end

      def init_log(payload, body, properties, metadata)
        { message: "After handling AMQP message",
          body: payload || body,
          protocol: "AMQP",
          consumer_tag: properties[:consumer_tag],
          redelivered: properties[:redelivered],
          exchange: properties[:exchange],
          routing_key: properties[:routing_key],
          content_type: metadata[:content_type],
          headers: metadata[:headers],
          delivery_mode: metadata[:delivery_mode] }
      end
    end
  end
end
