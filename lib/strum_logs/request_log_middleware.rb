# frozen_string_literal: true

require "time"
require "strum_logs/configuration"
require "strum_logs/helpers/http_log_helper"

module StrumLogs
  module Rack
    class RequestLogMiddleware
      include StrumLogs::Configuration
      include StrumLogs::Helpers::HttpLogHelper

      def initialize(app)
        @app = app
        @logger = DefaultLogger.instance.logger
        @tracer = DefaultTelemetry.instance.tracer
      end

      def propagation_context(env)
        getter = OpenTelemetry::Common::Propagation.rack_env_getter
        OpenTelemetry.propagation.extract(env, getter: getter)
      end

      def call(env)
        OpenTelemetry::Context.with_current(propagation_context(env)) do
          @tracer.in_span(env["PATH_INFO"], kind: :server) do |span|
            logging_process(env, span)
          end
        end
      end

      def logging_process(env, span)
        log_entity = init_log(env)
        status, headers, body = @app.call(env)
        log_entity[:status] = status
        log_entity[:headers] = headers
        log_entity[:response_message] = parse_body(body)
        log_status(log_entity)
        [status, headers, body]
      rescue StandardError => e
        error_process(log_entity, e, span)
      ensure
        propagate(headers)
        logg_process(log_entity, env)
      end

      private

      def propagate(headers)
        return unless StrumLogs::Configuration.config.enable_export_spans

        setter = OpenTelemetry::Context::Propagation.text_map_setter
        OpenTelemetry.propagation.inject(headers, setter: setter)
      end

      def log_status(log_entity)
        success?(log_entity[:status]) ? log_entity.merge!({ log_status: "success" }) : log_entity.merge!({ log_status: "error" })
      end

      def success?(status)
        SUCCESSFUL_STATUSES.include?(status)
      end

      def init_log(env)
        {
          method: env["REQUEST_METHOD"],
          path: env["PATH_INFO"],
          query: query_string(env),
          protocol: env["SERVER_PROTOCOL"],
          started_at: Time.now,
          peer: env["HTTP_X_FORWARDED_FOR"] || env["REMOTE_ADDR"],
          message: "HTTP Request",
          user: env["REMOTE_USER"],
          server_name: env["SERVER_NAME"]
        }
      end

      def query_string(env)
        env["QUERY_STRING"] == "" ? nil : env["QUERY_STRING"]
      end
    end
  end
end
