# frozen_string_literal: true

require "strum_logs/configuration"
require "strum_logs/default_logger"
require "redis"

class Redis
  class Client
    def call(command)
      reply = process([command]) { read }
      StrumLogs::Configuration.config.redis_after_call_hooks.each { |hook| hook.call(command, reply) }
      raise reply if reply.is_a?(CommandError)

      if block_given? && reply != "QUEUED"
        yield reply
      else
        reply
      end
    end

    def log(command, reply)
      logger = StrumLogs::DefaultLogger.instance.logger
      logs = { request: command.join(" "), message: "redis request" }
      logger.error(logs.merge({ error: reply })) && return if reply.is_a?(CommandError)

      logger.info(logs.merge({ response_message: reply }))
    end
  end
end
