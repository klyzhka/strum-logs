# frozen_string_literal: true

module StrumLogs
  module Errors
    class Configuration < StandardError;
      def initialize(msg = "Unexpected error", exception_type="Configuration error")
        super msg
      end
    end
  end
end
