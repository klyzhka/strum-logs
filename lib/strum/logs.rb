# frozen_string_literal: true

require_relative "logs/version"

module Strum
  module Logs
    class Error < StandardError; end
    # Your code goes here...
  end
end
